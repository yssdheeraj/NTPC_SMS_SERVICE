package com.yss.SmsService.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DataProviderService {

	@Value(value = "${connection.data.key}")
	String connectiondatakey;

	boolean CREDENTIAL_STATUS;

	/* *********************** License Check ***********************************/
	public boolean checkCredentialStatus() {
		String xdate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String d1 = connectiondatakey.substring(2, 4);

		String d2 = connectiondatakey.substring(4, 8);

		String d3 = connectiondatakey.substring(10, 12);

		String dt = d1 + d3 + d2;
		System.out.println("dt : " + dt);
		System.out.println("xdate : " + xdate);
		long sd1 = Long.parseLong(dt);
		long sd2 = Long.parseLong(xdate);

		if (sd2 <= sd1) {
			CREDENTIAL_STATUS = true;
			return CREDENTIAL_STATUS;
		} else {

			CREDENTIAL_STATUS = false;
			return CREDENTIAL_STATUS;
		}
	}
	
}
