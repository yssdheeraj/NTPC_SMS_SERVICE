package com.yss.SmsService.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yss.SmsService.dao.IGenericDao;

@Service
@Transactional
public class GenericServiceImpl<T> implements IGenericService<T> {

	@Autowired
	private IGenericDao<T> iGenericDao;
	
	@Override
	public List<T> fetch(T t) {
		
		return iGenericDao.fetch(t);
		 
	}

	@Override
	public List<T> fetch(T t, String condition) {
		return iGenericDao.fetch(t, condition);
		
	}

	

	@Override
	public T find(T t, String condition) {
		return (T) iGenericDao.fetch(t, condition);
		
	}

	@Override
	public T create(T t) {
		return iGenericDao.create(t);
		
	}

	@Override
	public T update(T t) {
		return iGenericDao.update(t);
		
	}

	@Override
	public void delete(T t) {
		iGenericDao.delete(t);

	}

	@Override
	public T find(T t, Long id) {
		return  iGenericDao.find(t, id);
		
	}




}
