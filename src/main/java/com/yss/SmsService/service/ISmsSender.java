package com.yss.SmsService.service;

public interface ISmsSender {

	
	public int send(Long fld_SMSID,String mobile,String smsText,String sentDate,String url_name,String loginId);
	
	//public void SendSmsFromURL();
	
	public void SendSmsFromURL(String url_name);
	
	public void DeleteSentMessage();

	
}
