package com.yss.SmsService.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.yss.SmsService.entity.InstanceMessageDetails;
import com.yss.SmsService.entity.ReportEntity;
import com.yss.SmsService.entity.ScheduleMessageEntity;
import com.yss.SmsService.entity.UrlEntity;
import com.yss.SmsService.utill.Constants;

@Service("ISmsSender")
@PropertySource("classpath:application.properties")
public class SmsSenderImpl implements ISmsSender {

	@Autowired
	private IGenericService<InstanceMessageDetails> instanceMessageDetailService;

	@Autowired
	private IGenericService<ReportEntity> reportEntityService;
	
	@Autowired
	private IGenericService<UrlEntity> urlEntityService;
	
	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService;

	private static final Logger log = LoggerFactory.getLogger(SmsSenderImpl.class);

	//@Value("${sms-url}")
	//private String smsUrl;

	String MOBILE = "";
	String loginId;
	String SMS_TEXT = "";
	Long SMS_ID = 0L;
	String SENT_DATE1="";
	String SENT_DATE2="";
	String URL_NAME="";
	@Override
	public void SendSmsFromURL(String url_name) {
		
		URL_NAME=url_name;
		log.info(" #### HELLO URL #### " + URL_NAME);
		log.info(" #### START #### ");
		
		
		ReportEntity rr = new ReportEntity();
		//, "where flag = true"
		List<InstanceMessageDetails> smsList = instanceMessageDetailService.fetch(new InstanceMessageDetails(), "where flag = true");

		log.info(" -------------------------------------- sms list"+smsList);
		for (InstanceMessageDetails instanceMessageDetails : smsList) {
			
			if (instanceMessageDetails !=null) {
				MOBILE = instanceMessageDetails.getMobile();
				SMS_TEXT = instanceMessageDetails.getSmsText();
				SMS_ID = instanceMessageDetails.getId();
				ScheduleMessageEntity scheduleMessageEntity=scheduleMessageEntityService.find(new ScheduleMessageEntity(),SMS_ID);
				log.info(SMS_ID+" "+scheduleMessageEntity);
				log.info("hhhhhhhhhhhhhhhhhhhhhhhhhhhh "+scheduleMessageEntity.getLoginId());
				loginId=scheduleMessageEntity.getLoginId();
				
				
	/*************************************************Format Of Date***************************************************************/			
				SENT_DATE1=instanceMessageDetails.getMessageDate();
				DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
				Date date = null;
				try {
					date = (Date)formatter.parse(SENT_DATE1);
					System.out.println("*********************************" +date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}        
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				//String SENT_DATE2 = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
				
				//String SENT_DATE2 = cal.get(Calendar.YEAR)+ "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);
				
				String schdeuledDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();
				System.out.println("*********************************formatedDate : " + schdeuledDate);   
				
	/****************************************************************************************************************/			
				
				send(SMS_ID,MOBILE, SMS_TEXT,schdeuledDate,URL_NAME,loginId);
				instanceMessageDetails.setFlag(false);
				instanceMessageDetailService.update(instanceMessageDetails);
			} else {
				log.info("end");
			}
			

		}

	}

	@Override
	public int send(Long fld_SMSID, String mobile, String smsText ,String sentDate,String url_name,String loginId) {
		ReportEntity re = new ReportEntity();
		
		
		try {
			URL_NAME = url_name;
			log.info("***************SMS URL Ready For Send SMS*****************" + URL_NAME);
			URL_NAME = URL_NAME.replace("XXXX", mobile).replace("YYYY", smsText);
			log.info("#####URL After Replace By Mobile And Message#######" + URL_NAME);
			String response = new RestTemplate().getForObject(URL_NAME, String.class);
			
			if (response.isEmpty() || response == null) {
				log.info("Response : " + Constants.FAIL);
				
			
				re.setFld_SMSID(fld_SMSID+"");
				re.setMobile(mobile);
				re.setSmsText(smsText);
				re.setFlag(false);
				re.setReponse(response);
				re.setScheduleDate(sentDate);
				re.setLoginId(loginId);
				log.info("******* Save in Table SmsHistory With Fail Response*******");
				reportEntityService.create(re);
				URL_NAME = URL_NAME.replace(mobile, "XXXX").replace(smsText, "YYYY");
				log.info("********Refresh URL*********" + URL_NAME);
				return 0;
			} else {

				log.info("Response : " + Constants.SUCCESS);
				re.setFld_SMSID(fld_SMSID+"");
				re.setMobile(mobile);
				re.setSmsText(smsText);
				re.setFlag(true);
				re.setReponse(response);
				re.setScheduleDate(sentDate);
				re.setLoginId(loginId);
				log.info("******* Save in Table SmsHistory With Success Reponse*******");
				reportEntityService.create(re);

				URL_NAME = URL_NAME.replace(mobile, "XXXX").replace(smsText, "YYYY");
				log.info("********Refresh URL*********" + URL_NAME);
				return 1;
			}

		} catch (Exception e) {
			System.out.println(" #### Oops! Error in Connection #### " + e.getMessage());
		}
		return 1;
	}

	public void DeleteSentMessage() {
		
		List<InstanceMessageDetails> listDeleteMessage = instanceMessageDetailService.fetch(new InstanceMessageDetails(), "WHERE flag=FALSE");
		for (InstanceMessageDetails instanceMessageDetails : listDeleteMessage) {
			
			if (instanceMessageDetails !=null) {
				instanceMessageDetailService.delete(instanceMessageDetails);
				log.info("SENT MESSAGE DELETED SUCCESSFULLY-----------------------------------");
			}
			
			log.info("SENT MESSAGE NOT AVAILABLE-----------------------------------");
		}
	}

	

	
	

}