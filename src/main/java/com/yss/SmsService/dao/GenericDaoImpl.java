package com.yss.SmsService.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class GenericDaoImpl<T> implements IGenericDao<T> {

	@PersistenceContext
	public EntityManager entityManager;

	@Override
	public List<T> fetch(T t) {
		return entityManager.createQuery("from " + t.getClass().getName()).getResultList();
	}

	@Override
	public List<T> fetch(T t, String condition) {
		// TODO Auto-generated method stub

		if (condition != null && condition != "") {
			try {
				System.out.println("----Fetch By All in If---- ");
				 List<T> lists = entityManager.createQuery("From "+t.getClass().getName()+" "+condition).getResultList();
				 return lists;
						
			} catch (Exception e) {
				System.out.println("----Error In Fetch By All ---- " + e.getMessage());
				return null;
			}
		} else {
			System.out.println("----Fetch By All in else ---- ");
			return entityManager.createQuery("From"+t.getClass().getName()).getResultList();
		}
	}

	
	@Override
	public T find(T t, String condition) {
		try {
			System.out.println("-------------Find By Condition-------------");
			return (T) entityManager.createQuery("from " + t.getClass().getName() + " " + condition)
					.getResultList().get(0);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("-------------ERROR-------------");
			return null;
		}

	}

	@Override
	public T create(T t) {
		entityManager.persist(t);
		return t;

	}

	@Override
	public T update(T t) {
		return entityManager.merge(t);
		
	}

	@Override
	public void delete(T t) {
		
		entityManager.remove(entityManager.contains(t) ? t : entityManager.merge(t));
		
	}

	@Override
	public T find(T t, Long id) {
		return (T) entityManager.find(t.getClass(), id);
			}



	

}
