package com.yss.SmsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication
//@EnableScheduling
@Configuration
@ComponentScan
@EnableAutoConfiguration  
public class SmsServiceApplication {

	private static final Logger log = LoggerFactory.getLogger(SmsServiceApplication.class);
	
   
	public static void main(String[] args) {
		SpringApplication.run(SmsServiceApplication.class, args);
		log.info(" #### Sms Application Running #### ");
		
	}
		
	@Bean
	TaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }
	
}
