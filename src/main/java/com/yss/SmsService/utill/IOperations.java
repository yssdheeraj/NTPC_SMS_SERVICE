package com.yss.SmsService.utill;

import java.io.Serializable;
import java.util.List;

public interface IOperations<T> {
	
	public List<T> fetch(T t);

	public List<T> fetch(T t, String condition);

	public T find(T t, Long id);
	
	public T find(T t, String condition);

	public T create(T t);

	public T update(T t);

	public void delete(T t);
	
	


}
