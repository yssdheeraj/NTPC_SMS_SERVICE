package com.yss.SmsService.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_urlgatewaydetails")
public class UrlGatewayEntity {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="ClientName", nullable = true, columnDefinition = "varchar(255) default ''")
	private String clientName;
	
	@Column(name="SentSms", nullable = true, columnDefinition = "varchar(255) default ''")
	private String sentSms;
	
	@Column(name="PurchasedOn", nullable = true, columnDefinition = "varchar(255) default ''")
	private String purchasedOn;
	
	@Column(name="GsmSenderId", nullable = true, columnDefinition = "varchar(255) default ''")
	private String gsmSenderId;

	@OneToOne
	@JoinColumn(name = "StartBy")
	private UserEntity loginId;
	
	@Column(name = "StopBy")
	private String stopBy;
	
	@Column(name = "StartTime") 
	private String startTime;
	
	@Column(name = "StopTime")  
	private String stopTime;

	@Column(name = "SmsService")  
	private String smsService;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getSentSms() {
		return sentSms;
	}

	public void setSentSms(String sentSms) {
		this.sentSms = sentSms;
	}

	public String getPurchasedOn() {
		return purchasedOn;
	}

	public void setPurchasedOn(String purchasedOn) {
		this.purchasedOn = purchasedOn;
	}

	public String getGsmSenderId() {
		return gsmSenderId;
	}

	public void setGsmSenderId(String gsmSenderId) {
		this.gsmSenderId = gsmSenderId;
	}

	public UserEntity getLoginId() {
		return loginId;
	}

	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}

	public String getStopBy() {
		return stopBy;
	}

	public void setStopBy(String stopBy) {
		this.stopBy = stopBy;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStopTime() {
		return stopTime;
	}

	public void setStopTime(String stopTime) {
		this.stopTime = stopTime;
	}

	public String getSmsService() {
		return smsService;
	}

	public void setSmsService(String smsService) {
		this.smsService = smsService;
	}
	


	public UrlGatewayEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UrlGatewayEntity [id=" + id + ", clientName=" + clientName + ", sentSms=" + sentSms + ", purchasedOn="
				+ purchasedOn + ", gsmSenderId=" + gsmSenderId + ", loginId=" + loginId + ", stopBy=" + stopBy
				+ ", startTime=" + startTime + ", stopTime=" + stopTime + ", smsService=" + smsService + "]";
	}

	public UrlGatewayEntity(long id, String clientName, String sentSms, String purchasedOn, String gsmSenderId,
			UserEntity loginId, String stopBy, String startTime, String stopTime, String smsService) {
		super();
		this.id = id;
		this.clientName = clientName;
		this.sentSms = sentSms;
		this.purchasedOn = purchasedOn;
		this.gsmSenderId = gsmSenderId;
		this.loginId = loginId;
		this.stopBy = stopBy;
		this.startTime = startTime;
		this.stopTime = stopTime;
		this.smsService = smsService;
	}

	
		
}
	
