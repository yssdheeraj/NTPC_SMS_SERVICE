package com.yss.SmsService.ScheduleController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yss.SmsService.entity.UrlEntity;
import com.yss.SmsService.entity.UrlGatewayEntity;
import com.yss.SmsService.service.DataProviderService;
import com.yss.SmsService.service.IGenericService;
import com.yss.SmsService.service.ISmsSender;

@Controller
@CrossOrigin
public class ScheduleController {

	private static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);

	// private static final SimpleDateFormat dateFormat = new
	// SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	// public static final CronTrigger EVERY_TEN_SECONDS = new CronTrigger("0/10 * *
	// * * *");
	
	@Autowired
	private DataProviderService dataProviderService;

	@Autowired
	private ISmsSender iSmsSender;

	@Autowired
	private IGenericService<UrlGatewayEntity> urlGatewayEntityService;

	@Autowired
	private IGenericService<UrlEntity> urlEntityService;

	public static final long FIXED_RATE = 10000;

	@Autowired
	TaskScheduler taskScheduler;

	ScheduledFuture<?> scheduledFuture;

	String URL_NAME = "";
	String CREDENTAIL_CHECK="Sms URL Stopped Due To License Expired";

	public boolean RESPONSE = false;

	@RequestMapping("/status/url")
	public ResponseEntity<?> reponse() {

		return new ResponseEntity<>(RESPONSE, HttpStatus.OK);
	}

	@PostMapping(value = "/start/smsSender/service")
	public ResponseEntity<?> start(@RequestBody UrlGatewayEntity objUrlGatewayEntityStart) {

		List<UrlEntity> objUrlEntity = new ArrayList<UrlEntity>();

		objUrlEntity = urlEntityService.fetch(new UrlEntity());
		for (UrlEntity urlEntity : objUrlEntity) {
			URL_NAME = urlEntity.getUrlName();
			logger.info("#### URL NAME fetch From tbl_url #### " + URL_NAME);
		}

		if (URL_NAME == null || URL_NAME.equals("")) {
			logger.info(" ##### URL_NAME IS NOT FOUND To Start Sms ###### ");
			boolean urlname=false;
			return new ResponseEntity<>(URL_NAME,HttpStatus.OK);
		} else {
			boolean checkCredential = dataProviderService.checkCredentialStatus();
			if (checkCredential) {
				scheduledFuture = taskScheduler.scheduleAtFixedRate(printHour(), FIXED_RATE);
				// scheduledFuture = taskScheduler.schedule(printHour(), EVERY_TEN_SECONDS);
				logger.info(" ##### URL_NAME IS FOUND ###### ");
				RESPONSE = true;
				logger.info("#### Save Data in URL GATEWAY Details #### " + objUrlGatewayEntityStart.toString());
				objUrlGatewayEntityStart.setStartTime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()).toString());
				urlGatewayEntityService.create(objUrlGatewayEntityStart);
				return new ResponseEntity<>(URL_NAME,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(CREDENTAIL_CHECK,HttpStatus.OK);
			}
			
		}

	

	}

	@PostMapping(value = "/stop/smsSender/service")
	public ResponseEntity<Void> stop() throws ExecutionException {

		URL_NAME = "";

		try {
			scheduledFuture.cancel(false);
			logger.info(
					" #### what is current status if stop false otherwise true #### " + scheduledFuture.cancel(false));
			boolean stopUrlResponse = scheduledFuture.isCancelled();
			logger.info("#### Stop URL Status #### " + stopUrlResponse);
			RESPONSE = false;
			// logger.info("#### Stop SMS URL Successfully At #### " + dateFormat.format(new
			// Date()));
			logger.info("#### Stop SMS URL Successfully At #### "
					+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (CancellationException ce) {
			logger.info("#### Excpetion in Stop Scheduler #### " + ce.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
		}
	}

	private Runnable printHour() {
		// return () -> System.out.println("Hello " + Instant.now().toEpochMilli());

		return () -> {
			try {
				TimeUnit.SECONDS.sleep(10);
				if (URL_NAME == null || URL_NAME.isEmpty()) {
					System.out.println(" #### Please Select a URL #### ");

				} else {
					
						logger.info("################################################################################");
						logger.info("#### Start SMS URL Successfully At #### "
								+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
						iSmsSender.SendSmsFromURL(URL_NAME);
						logger.info("#### Message Sending ####");
						iSmsSender.DeleteSentMessage();
						logger.info("#### Message Deleting ####");
						logger.info("################################################################################");
					
				}
			} catch (InterruptedException e) {

				logger.info(" #### Exception in Time Delay #### " + e.getMessage());
			}

		};

	}

}
